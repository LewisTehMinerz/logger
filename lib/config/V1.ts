/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogLevel } from '../constants';
import { TransportOptions, TransportOptionsLogger } from '../transports';

/**
 * @ignore
 */
enum V1LogLevel {
	OFF = 'OFF',
	ERROR = 'ERROR',
	WARN = 'WARN',
	INFO = 'INFO',
	DEBUG = 'DEBUG',
	TRACE = 'TRACE',
}

/**
 * @ignore
 */
interface V1Logger {
	name: string;
	level: V1LogLevel;
	exact?: boolean;
}

/**
 * @ignore
 */
declare const global: {
	'AYANA_LOGGER_CONFIG'?: {
		level?: string;
		loggers?: Array<V1Logger>;
	}
} & NodeJS.Global;

export class V1 {
	public static setConfig(config: TransportOptions & { formatter?: never }) {
		global.AYANA_LOGGER_CONFIG = {
			level: V1.remapLevel(config.level),
			loggers: V1.remapLoggers(config.loggers),
		};
	}

	private static remapLoggers(loggers: Array<TransportOptionsLogger>): Array<V1Logger> {
		const v1Loggers: Array<V1Logger> = [];
		if (Array.isArray(loggers)) {
			for (const logger of loggers) {
				v1Loggers.push({
					level: V1.remapLevel(logger.level),
					name: logger.name,
					exact: logger.exact,
				});
			}
		}

		return v1Loggers;
	}

	private static remapLevel(level: LogLevel): V1LogLevel {
		switch (level) {
			case LogLevel.OFF: return V1LogLevel.OFF;
			case LogLevel.ERROR: return V1LogLevel.ERROR;
			case LogLevel.WARN: return V1LogLevel.WARN;
			case LogLevel.DEBUG: return V1LogLevel.DEBUG;
			case LogLevel.TRACE: return V1LogLevel.TRACE;
			default: return V1LogLevel.INFO;
		}
	}
}
