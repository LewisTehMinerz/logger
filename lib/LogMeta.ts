/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogLevel } from './constants';
import { Logger } from './Logger';

/**
 * Logging metadata passed to all transports and formatters
 */
export interface LogMeta {
	/**
	 * The instance of the [[Logger]] that initiated the current logging call.
	 */
	readonly origin: Logger;
	/**
	 * The [[LogLevel]] of the message.
	 */
	readonly level: LogLevel;
	/**
	 * The optional uniqueMarker used to denote multiple instance.
	 */
	readonly uniqueMarker?: string;
	/**
	 * The input message. This can either be a string or an Error.
	 */
	readonly input: string | Error;
	/**
	 * An optional object of key value pairs with extra data (Can be used for a central logging system for example).
	 */
	readonly extra?: { [key: string]: any };
}
