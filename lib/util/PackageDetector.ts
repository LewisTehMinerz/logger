/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as fs from 'fs';
import * as path from 'path';

/**
 * @ignore
 */
export class PackageDetector {
	private readonly packageCache: Map<string, any> = new Map();

	public getRootOf(directory: string) {
		// Find the next package.json file in the directory tree
		let files = this.readdirSync(directory);
		while (!files.includes('package.json')) {
			const up = path.join(directory, '..');

			// If there is no package.json we will get stuck at the root directory
			if (up === directory) {
				directory = null;
				break;
			}

			directory = up;
			files = this.readdirSync(directory);
		}

		if (directory == null) throw new Error('No package.json could be found in the directory tree');

		return directory;
	}

	public getInfo(rootDir: string) {
		// Get package.json location
		const packageFile = path.resolve(rootDir, 'package.json');

		// Load package.json from cache or require it
		let pkg;
		if (this.packageCache.get(packageFile) == null) {
			pkg = this.require(packageFile);
			this.packageCache.set(packageFile, pkg);
		} else {
			pkg = this.packageCache.get(packageFile);
		}

		return pkg;
	}

	public getCallerDirectory() {
		const stack = this.getCallStack();
		// 0: Error line, 1: Call to getCallStack(), 2: Call to getCallerDirectory(), 3: Call to our caller
		// tslint:disable-next-line: no-magic-numbers
		const callerFile = stack[3].getFileName();
		const callerDirname = path.dirname(callerFile);

		return callerDirname;
	}

	// Wrapper functions so we can test this without context

	/* istanbul ignore next */
	private getCallStack(): Array<NodeJS.CallSite> {
		const pst = Error.prepareStackTrace;

		Error.prepareStackTrace = (e, stk) => {
			Error.prepareStackTrace = pst;

			return stk;
		};

		return (new Error() as any).stack;
	}

	/* istanbul ignore next */
	private readdirSync(dirPath: fs.PathLike) {
		return fs.readdirSync(dirPath);
	}

	/* istanbul ignore next */
	private require(id: string) {
		return require(id);
	}
}
