// tslint:disable: variable-name no-magic-numbers

// CodeMap taken from https://github.com/Marak/colors.js/blob/master/lib/styles.js
// Licensed under MIT - Copyright (c) Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
/**
 * @ignore
 */
const CodeMap: { [key: string]: [string | number, string | number] } = {
	reset: [0, 0],

	bold: [1, 22],
	dim: [2, 22],
	italic: [3, 23],
	underline: [4, 24],
	inverse: [7, 27],
	hidden: [8, 28],
	strikethrough: [9, 29],

	black: [30, 39],
	red: [31, 39],
	green: [32, 39],
	yellow: [33, 39],
	blue: [34, 39],
	magenta: [35, 39],
	cyan: [36, 39],
	white: [37, 39],
	gray: [90, 39],

	bgBlack: [40, 49],
	bgRed: [41, 49],
	bgGreen: [42, 49],
	bgYellow: [43, 49],
	bgBlue: [44, 49],
	bgMagenta: [45, 49],
	bgCyan: [46, 49],
	bgWhite: [47, 49],
};

Object.values(CodeMap).forEach(value => {
	value[0] = `\u001b[${value[0]}m`;
	value[1] = `\u001b[${value[1]}m`;
});

/**
 * @ignore
 */
export class Color {
	private disabled: boolean = false;

	public setEnabled(enabled: boolean) {
		this.disabled = !enabled;
	}

	public setDisabled(disabled: boolean) {
		this.disabled = disabled;
	}

	public get(color: string, str: string) {
		if (this.disabled) return str;

		return `${CodeMap[color][0]}${str}${CodeMap[color][1]}`;
	}

	public green(str: string) {
		return this.get('green', str);
	}

	public blue(str: string) {
		return this.get('blue', str);
	}

	public gray(str: string) {
		return this.get('gray', str);
	}

	public red(str: string) {
		return this.get('red', str);
	}

	public dim(str: string) {
		return this.get('dim', str);
	}

	public cyan(str: string) {
		return this.get('cyan', str);
	}

	public yellow(str: string) {
		return this.get('yellow', str);
	}

	public magenta(str: string) {
		return this.get('magenta', str);
	}

	public underline(str: string) {
		return this.get('underline', str);
	}

	public bgRed(str: string) {
		return this.get('bgRed', str);
	}
}
