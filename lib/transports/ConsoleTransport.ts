/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as os from 'os';

import { LogLevel, LogLevelValue } from '../constants';
import { LogMeta } from '../LogMeta';

import { Transport, TransportOptions } from './Transport';

export interface ConsoleTransportOptions extends TransportOptions {
	eol?: string;
	stderrMinLevel?: LogLevel;
}

export class ConsoleTransport extends Transport<ConsoleTransportOptions> {
	public constructor(options: ConsoleTransportOptions = {}) {
		options = {
			eol: os.EOL,
			stderrMinLevel: LogLevel.WARN,
			...options,
		};

		super(options);
	}

	public print(meta: LogMeta, message: string) {
		if (LogLevelValue[meta.level] > LogLevelValue[this.options.stderrMinLevel]) {
			if ((console as any)._stdout) {
				process.stdout.write(`${message}${this.options.eol}`);
			} else {
				// console.log adds a newline
				console.log(message);
			}
		} else if ((console as any)._stderr) {
			process.stderr.write(`${message}${this.options.eol}`);
		} else {
			// console.error adds a newline
			console.error(message);
		}
	}
}
