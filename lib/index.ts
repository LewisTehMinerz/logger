/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Logger } from './Logger';

/**
 * Alias for {@link Logger.get}.
 *
 * @see {@link Logger.get}
 */
const get = Logger.get;
export { Logger, get };
export default Logger;

export * from './LogMeta';

export * from './config/V1';

export * from './constants';
export * from './formatter';
export * from './transports';
