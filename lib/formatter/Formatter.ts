/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogMeta } from '../LogMeta';

export abstract class Formatter {
	public format(meta: LogMeta): string {
		const anyMeta: any = meta;

		if (this.STORAGE_KEY != null) {
			if (anyMeta[this.STORAGE_KEY] != null) return anyMeta[this.STORAGE_KEY];
		}

		let msg;
		if (meta.input instanceof Error) {
			msg = this.formatError(meta, meta.input);
		} else {
			msg = this.formatMessage(meta, meta.input);
		}

		if (this.STORAGE_KEY != null) {
			anyMeta[this.STORAGE_KEY] = msg;
		}

		return msg;
	}

	protected abstract STORAGE_KEY?: symbol;

	protected abstract formatError(meta: LogMeta, error: Error): string;
	protected abstract formatMessage(meta: LogMeta, message: string): string;
}
