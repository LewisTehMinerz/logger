/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { GenericError } from '@ayana/errors';

/**
 * @ignore
 */
const fecha = require('fecha');

/**
 * @ignore
 */
let genericError: typeof GenericError;
try {
	require.resolve('@ayana/errors');
	genericError = require('@ayana/errors').GenericError;
} catch (e) {
	// Ignore
}

import { LogLevelColor } from '../constants';
import { LogMeta } from '../LogMeta';
import { Color } from '../util';

import { Formatter } from './Formatter';

/**
 * @ignore
 */
const DEFAULT_FORMATTER_STORAGE_KEY = Symbol('DefaultFormatter');

export interface DefaultFormatterOptions {
	dateFormat?: string;
	colorErrors?: boolean;
	colorMeta?: boolean;
}

export class DefaultFormatter extends Formatter {
	private readonly errorColorer = new Color();
	private readonly logColorer = new Color();

	private readonly options: DefaultFormatterOptions;

	protected readonly STORAGE_KEY = DEFAULT_FORMATTER_STORAGE_KEY;

	public constructor(options?: DefaultFormatterOptions) {
		super();

		options = options || {};
		if (typeof options.dateFormat !== 'string') options.dateFormat = 'YYYY-MM-DD HH:mm:ss:SSS';
		if (typeof options.colorErrors !== 'boolean') options.colorErrors = true;
		if (typeof options.colorMeta !== 'boolean') options.colorMeta = true;

		this.options = options;

		this.logColorer.setEnabled(this.options.colorMeta);
	}

	protected formatMessage(meta: LogMeta, message: string) {
		const timestamp = fecha.format(Date.now(), this.options.dateFormat);

		// tslint:disable-next-line: no-magic-numbers
		const coloredLevel = this.logColorer.get(LogLevelColor[meta.level as any], meta.level.padEnd(6));
		const location = `[${this.logColorer.green(`${meta.origin.packageName}:`)}${this.logColorer.blue(`${meta.origin.packagePath}${meta.origin.name}`)}${meta.uniqueMarker ? `/${this.logColorer.gray(`${meta.uniqueMarker}`)}` : ''}]`;

		return `${timestamp} ${coloredLevel}${location}: ${message}`;
	}

	protected formatError(meta: LogMeta, error: Error): string {
		let message = this.colorError(error);

		if (genericError != null && error instanceof genericError) {
			let cause: any = error;
			do {
				cause = cause.getCause();

				if (cause != null) {
					message += `${this.getCausePrefix()} ${this.colorError(cause)}`;
				}
			} while (cause instanceof genericError);
		}

		return this.formatMessage(meta, message);
	}

	private getCausePrefix() {
		if (this.options.colorErrors) {
			return `\n ${this.errorColorer.get('bgRed', 'Caused by:')}`;
		}

		return `\n Caused by:`;
	}

	private colorError(error: Error | GenericError) {
		if (!this.options.colorErrors) return error.stack;

		let coloredError = error.stack;

		if (error instanceof genericError && error.hasCode()) {
			coloredError = coloredError.replace(`${error.name}:`, `${error.name}: (${this.errorColorer.magenta(String(error.getCode()))})`);
		}

		return coloredError
			.replace(error.name, this.errorColorer.underline(error.name))
			.replace(/at (.*?) \(/g, `at ${this.errorColorer.cyan('$1')} (`)
			.replace(/at (\/.*:.*:.*)/g, `at ${this.errorColorer.dim('$1')}`)
			.replace(/(\n {4}at)/g, this.errorColorer.yellow('$1'))
			.replace(/\((.*:.*:.*|native|eval at.*|unknown location)\)/g, this.errorColorer.dim('($1)'));
	}
}
