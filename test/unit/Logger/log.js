'use strict';

const { Config } = require('../../../build/config/Config');
const { Logger } = require('../../../build/Logger');
const { LogLevel } = require('../../../build/constants/LogLevel');

describe('#constructor', function() {
	const getClean = () => {
		const tested = new Logger(undefined, undefined, undefined, {});

		return tested;
	};

	it('should pass all arguments and itself to all transports', function() {
		const tested = getClean();

		const fake = sinon.fake();
		Config.getInstance().transports = [
			{
				log: fake,
			}, {
				log: fake,
			},
		];

		const mockData = {
			level: LogLevel.INFO,
			message: 'TestMessage',
			uniqueMarker: 'TestUniqueMarker',
		};

		tested.log(mockData.level, mockData.message, mockData.uniqueMarker);

		sinon.assert.calledTwice(fake);

		for (let i = 0; i < 2; i++) {
			expect(fake.args[i][0], 'to be', tested);
			expect(fake.args[i][1], 'to be', mockData.level);
			expect(fake.args[i][2], 'to be', mockData.message);
			expect(fake.args[i][3], 'to be', mockData.uniqueMarker);
		}
	});

	it('should create a new meta object using the passed extra and the extra defined in the logger', function() {
		const tested = getClean();

		const fake = sinon.fake();
		Config.getInstance().transports = [
			{
				log: fake,
			},
		];

		tested.extra.loggerExtra = true;
		const passedExtra = { calledExtra: true };

		tested.log(undefined, undefined, undefined, passedExtra);

		sinon.assert.calledOnce(fake);

		expect(fake.args[0][4].calledExtra, 'to be true');
		expect(fake.args[0][4].loggerExtra, 'to be true');
		expect(fake.args[0][4], 'not to be', tested.extra);
		expect(fake.args[0][4], 'not to be', passedExtra);
	});

	it('should take generated metadata from a transport and reuse it', function() {
		const tested = getClean();

		const meta = {};
		const transports = [
			{
				log: sinon.fake.returns(meta),
			}, {
				log: sinon.fake(),
				logMeta: sinon.fake(),
			},
		];

		Config.getInstance().transports = transports;

		tested.log();

		sinon.assert.calledOnce(transports[0].log);
		sinon.assert.notCalled(transports[1].log);
		sinon.assert.calledOnce(transports[1].logMeta);

		expect(transports[1].logMeta.args[0][0], 'to be', meta);
	});
});
