'use strict';

const { Logger } = require('../../../build/Logger');
const { PackageDetector } = require('../../../build/util/PackageDetector');

describe('#get', function() {
	const setupDetector = (callerDir, packageInfo) => {
		Logger.detector.getCallerDirectory = sinon.fake.returns(callerDir);
		Logger.detector.getRootOf = sinon.fake.returns('/');
		Logger.detector.getInfo = sinon.fake.returns(packageInfo);
	};

	it('should detect the logger name correctly', function() {
		setupDetector('/src/sub/package', { main: '/src/index.js' });

		expect(Logger.get('TestLogger').name, 'to be', 'TestLogger');
		expect(Logger.get().name, 'to be', '');
		expect(Logger.get(null).name, 'to be', '');
		expect(Logger.get('').name, 'to be', '');
		expect(Logger.get('index.js').name, 'to be', '');
	});

	it('should detect the packages name correctly', function() {
		setupDetector('/src/sub/package', { main: '/src/index.js', name: 'test-package' });

		expect(Logger.get('TestLogger').packageName, 'to be', 'test-package');
	});

	it('should detect the package path correctly using the packages main entry', function() {
		setupDetector('/src/sub/package', { main: '/src/index.js' });

		expect(Logger.get('TestLogger').packagePath, 'to be', 'sub.package.');
	});

	it('should detect the package path correctly using the packages loggerBase entry', function() {
		setupDetector('/src/sub/package', { loggerBase: '/src/sub', main: '/src/index.js' });

		expect(Logger.get('TestLogger').packagePath, 'to be', 'package.');
	});

	it('should throw an error if the caller file is outside of the packages main directory', function() {
		setupDetector('/test/package', { main: 'src/index.js' });

		expect(() => Logger.get('TestLogger'), 'to throw', 'Logger.get(): This file is not in the logger base path for this project');
	});

	it('should not end the package path with a dot if it is empty', function() {
		setupDetector('/src/', { main: '/src/index.js' });

		expect(Logger.get('').packagePath, 'to be', '');
		expect(Logger.get('TestLogger').packagePath, 'to be', '');
	});

	it('should not end the package path with a dot if the loggers name is empty or index.js', function() {
		setupDetector('/src/sub/package', { main: '/src/index.js' });

		expect(Logger.get('').packagePath, 'to be', 'sub.package');
		expect(Logger.get('index.js').packagePath, 'to be', 'sub.package');
	});

	it('should add the extra object', function() {
		setupDetector('/src/sub/package', { main: '/src/index.js' });

		const extra = {};
		expect(Logger.get('', extra).extra, 'to be', extra);
	});

	// eslint-disable-next-line no-undef
	after(function() {
		// Cleanup package detector
		Logger.detector = new PackageDetector();
	});
});
