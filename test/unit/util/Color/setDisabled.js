'use strict';

const { Color } = require('../../../../build/util/Color');

describe('#setDisabled', function() {
	const getClean = () => {
		const tested = new Color();

		return tested;
	};

	it('should set the disabled status according to the input', function() {
		const tested = getClean();

		tested.setDisabled(true);
		expect(tested.disabled, 'to be true');

		tested.setDisabled(false);
		expect(tested.disabled, 'to be false');
	});
});
