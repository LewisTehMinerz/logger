'use strict';

const { Color } = require('../../../../build/util/Color');

const checkDelegate = colorName => {
	const color = new Color();

	color.get = sinon.fake();

	const mockString = 'MockString';

	color[colorName](mockString);

	sinon.assert.calledOnce(color.get);

	expect(color.get.args[0][0], 'to be', colorName);
	expect(color.get.args[0][1], 'to be', mockString);
};

[
	'green',
	'blue',
	'gray',
	'red',
	'dim',
	'cyan',
	'yellow',
	'underline',
	'bgRed',
].forEach(color => {
	describe(`#${color}`, function() {
		it('should forward all parameters and attach the correct color', function() {
			checkDelegate(color);
		});
	});
});
