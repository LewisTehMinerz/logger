'use strict';

const { PackageDetector } = require('../../../../build/util/PackageDetector');

describe('#getInfo', function() {
	const getClean = () => {
		const tested = new PackageDetector();

		tested.require = sinon.fake();

		return tested;
	};

	it('should load the package.json file if it is not in the cache, add it to the cache and return it', function() {
		const tested = getClean();

		const pkg = {};

		tested.require = sinon.fake.returns(pkg);

		const result = tested.getInfo('/');

		expect(result, 'to be', pkg);
		expect(tested.packageCache.get('/package.json'), 'to be', pkg);

		sinon.assert.calledOnce(tested.require);
	});

	it('should not load the package.json file if it is in the cache and return it', function() {
		const tested = getClean();

		const pkg = {};
		tested.packageCache.set('/package.json', pkg);

		const result = tested.getInfo('/');

		expect(result, 'to be', pkg);

		sinon.assert.notCalled(tested.require);
	});
});
