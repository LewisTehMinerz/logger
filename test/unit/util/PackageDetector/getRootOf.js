'use strict';

const { PackageDetector } = require('../../../../build/util/PackageDetector');

describe('#getRootOf', function() {
	const getClean = () => {
		const tested = new PackageDetector();

		tested.readdirSync = sinon.fake();

		return tested;
	};

	it('should find the next folder containing a package.json file', function() {
		const tested = getClean();

		const expectedPath = '/home/ayana/Workspaces/ayana/logger';

		tested.readdirSync = function(path) {
			if (path === expectedPath) {
				return ['package.json'];
			}

			return [];
		};

		const root = tested.getRootOf('/home/ayana/Workspaces/ayana/logger/some/very/fancy/dir');

		expect(root, 'to be', expectedPath);
	});

	it('should abort after checking the root directory for a package.json', function() {
		const tested = getClean();

		tested.readdirSync = sinon.fake.returns([]);

		expect(
			() => tested.getRootOf('/'),
			'to throw',
			'No package.json could be found in the directory tree'
		);
	});
});
