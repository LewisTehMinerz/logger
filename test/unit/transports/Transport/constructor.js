'use strict';

const { Transport } = require('../../../../build/transports/Transport');
const { LogLevel } = require('../../../../build/constants/LogLevel');

describe('#constructor', function() {
	let setLevel, setFormatter, setLoggers;
	before(function() {
		setLevel = Transport.prototype.setLevel;
		setFormatter = Transport.prototype.setFormatter;
		setLoggers = Transport.prototype.setLoggers;
	});

	it('should take the input options and call the corresponding methods', function() {
		const options = {
			level: LogLevel.DEBUG,
			formatter: {},
			loggers: {},
		};

		Transport.prototype.setLevel = sinon.fake();
		Transport.prototype.setFormatter = sinon.fake();
		Transport.prototype.setLoggers = sinon.fake();

		// eslint-disable-next-line no-new
		new Transport(options);

		sinon.assert.calledOnce(Transport.prototype.setLevel);
		sinon.assert.calledOnce(Transport.prototype.setFormatter);
		sinon.assert.calledOnce(Transport.prototype.setLoggers);

		expect(Transport.prototype.setLevel.args[0][0], 'to be', options.level);
		expect(Transport.prototype.setFormatter.args[0][0], 'to be', options.formatter);
		expect(Transport.prototype.setLoggers.args[0][0], 'to be', options.loggers);
	});

	afterEach(function() {
		Transport.prototype.setLevel = setLevel;
		Transport.prototype.setFormatter = setFormatter;
		Transport.prototype.setLoggers = setLoggers;
	});
});
